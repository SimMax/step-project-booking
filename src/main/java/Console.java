import dao.DAOFactory;
import dao.Identifiable;
import entities.Booking;
import entities.Flight;
import entities.Person;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Console {
    private DAOFactory<Identifiable> dao_people;
    private DAOFactory<Identifiable> dao_flights;
    private DAOFactory<Identifiable> dao_booking;
    private Person user;

    public Console(DAOFactory<Identifiable> dao_people, DAOFactory<Identifiable> dao_flights, DAOFactory<Identifiable> dao_booking) {
        this.dao_people = dao_people;
        this.dao_flights = dao_flights;
        this.dao_booking = dao_booking;
    }

    public void start(){
        user = authorization();
        while (true){
            int usersNumber = getUsersMenuNumber();
            if(usersNumber == 1){
                printOnlineScoreboard();
            } else if(usersNumber == 2){
                printFlightInformation();
            } else if(usersNumber == 3){
                ticketBooking();
            } else if(usersNumber == 4){
                cancellationBooking();
            } else if(usersNumber == 5){
                printMyFlights();
            } else if(usersNumber == 6){
                user = authorization();
            } else if(usersNumber == 7){
                dao_people.writeToFile();
                dao_flights.writeToFile();
                dao_booking.writeToFile();
                System.out.println("Спасибо, до свидания!");
                break;
            }
        }
    }

    private Person authorization(){
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            while (true){
                System.out.println("Введите ваш login:");
                String login = reader.readLine();
                System.out.println("Введите ваш пароль:");
                String pwd = reader.readLine();
                for (Identifiable item : dao_people.getAll()) {
                    Person person = (Person) item;
                    if (person.getLogin().equals(login) && person.getPwd().equals(pwd)) {
                        return person;
                    }
                }
                System.out.println("Совпадения не найдены! Попробуйте еще.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private int getUsersMenuNumber(){
        printMenu();
        System.out.println("Введите пункт меню (с 1 до 7): ");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int number;
        while (true){
            try {
                String str = reader.readLine();
                if(isNumeric(str)){
                    number = Integer.parseInt(str);
                    if(number >= 1 && number <= 7){
                        return number;
                    } else {
                        System.out.println("Ошибка! Введите пункт меню (с 1 до 7): ");
                    }
                } else {
                    System.out.println("Ошибка! Введите пункт меню (с 1 до 7): ");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean isNumeric(String str) {
        try {
           Integer.parseInt(str);
        } catch(NumberFormatException e) {
            return false;
        }
        return true;
    }

    private void printOnlineScoreboard(){
        Date today = new Date();
        dao_flights.getAll().forEach(item -> {
            Flight flight = (Flight) item;
            long takeoffTime = flight.getTakeoffTime();
            long millisecondsPerDay = 86400000;
            if((takeoffTime > today.getTime()) && (takeoffTime - today.getTime() < millisecondsPerDay)){
                System.out.println(flight);
            }
        });
    }

    private void printFlightInformation(){
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите id рейса:");
        try {
            String str = reader.readLine();
            if (isNumeric(str)){
                System.out.println(dao_flights.get(Integer.parseInt(str)));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void ticketBooking(){
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Введите место вылета:");
            String takeoffPlace = reader.readLine();
            System.out.println("Введите место назначения:");
            String landingPlace = reader.readLine();
            System.out.println("Введите дату (формат даты 31/12/2012):");
            String dateStr = reader.readLine();

            DateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
            Date date = null;
            try {
                date = format.parse(dateStr);
            } catch (ParseException e) {
                System.out.println("Неверный формат даты: " + dateStr);
                return;
            }

            System.out.println("Подходящие рейсы:");
            ArrayList<Flight> filteredFlights = new ArrayList<>();
            Date finalDate = date;

            dao_flights.getAll().forEach(item -> {
                Flight flight = (Flight) item;
                if(flight.getFrom().equals(takeoffPlace)
                        && flight.getTo().equals(landingPlace)
                        && Math.abs(flight.getTakeoffTime() - finalDate.getTime()) < 86400000){
                    System.out.println(flight);
                    filteredFlights.add(flight);
                }
            });

            System.out.println("Введите id рейса для брони:");
            String idForBooking = reader.readLine();
            if(isNumeric(idForBooking)){
                int id = Integer.parseInt(idForBooking);
                filteredFlights.forEach(item -> {
                    if(id == item.getId()){
                        System.out.printf("Бронь, рейса id:%s, успешна.\n", item.getId());
                        user.addBooking(item.getId());

                        Flight flight = (Flight)dao_flights.get(item.getId());
                        flight.setEmptySeats(flight.getEmptySeats() - 1);

                        Booking booking = (Booking)dao_booking.get(item.getId());
                        booking.addPassenger(user.getId());
                    }
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void printMyFlights(){
        List<Integer> bookings = user.getBookings();
        System.out.println("Ваши рейсы:");
        bookings.forEach(item -> {
            System.out.println(dao_flights.get(item));
        });
    }

    private void cancellationBooking(){
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        printMyFlights();
        System.out.println("Введите id рейса для отмены брони:");
        try {
            String str = reader.readLine();
            if (isNumeric(str)){
                List<Integer> bookings = user.getBookings();
                int flightsId = Integer.parseInt(str);
                for(int i = 0; i < bookings.size(); i++)
                    if (bookings.get(i) == flightsId) {
                        bookings.remove(i);

                        Flight flight = (Flight) dao_flights.get(flightsId);
                        flight.setEmptySeats(flight.getEmptySeats() + 1);

                        Booking booking = (Booking) dao_booking.get(flightsId);
                        booking.delPassenger(user.getId());

                        System.out.printf("Бронь, рейса id:%s, отменена.\n", flightsId);
                        break;
                    }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void printMenu(){
        System.out.println();
        System.out.printf("----Меню--------%s-%s---\n", user.getSurname(), user.getName());
        System.out.println("1. Онлайн-табло.");
        System.out.println("2. Посмотреть информацию о рейсе.");
        System.out.println("3. Поиск и бронировка рейса.");
        System.out.println("4. Отменить бронирование.");
        System.out.println("5. Мои рейсы.");
        System.out.println("6. Завершить сессию.");
        System.out.println("7. Выход.");
        System.out.println("--------------------------------");
    }
}

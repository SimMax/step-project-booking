import dao.DAOFactory;
import dao.Identifiable;
import daoimpl.DAOHashMapBooking;
import daoimpl.DAOHashMapFlights;
import daoimpl.DAOHashMapPeople;

public class DAOApp {
    public static void main(String[] args) {
        DAOFactory<Identifiable> dao_people = new DAOHashMapPeople();
        DAOFactory<Identifiable> dao_flights = new DAOHashMapFlights();
        DAOFactory<Identifiable> dao_booking = new DAOHashMapBooking();
        Console console = new Console(dao_people, dao_flights, dao_booking);
        console.start();
    }
}